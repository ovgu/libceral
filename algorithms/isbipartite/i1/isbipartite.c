#include <stdlib.h>
#include <ceral.h>
#include <math.h>
#include "../raw/isbipartite.c"

CborError isbipartite_encode_params(CborEncoder *encoder, void *params) {
    CborError err = CborNoError;

    Graph *g = (Graph*) params;
    CborEncoder arrEncoder;
    err = cbor_encoder_create_array(encoder, &arrEncoder, 1 + (g->edge_count * 2));
    if (err != CborNoError) goto error;
    err = cbor_encode_uint(&arrEncoder, g->node_count);
    if (err != CborNoError) goto error;
    for (size_t i = 0; i < g->edge_count; i++) {
        err = cbor_encode_uint(&arrEncoder, g->edges[i].from);
        if (err != CborNoError) goto error;
        err = cbor_encode_uint(&arrEncoder, g->edges[i].to);
        if (err != CborNoError) goto error;
    }
    err = cbor_encoder_close_container(encoder, &arrEncoder);
    if (err != CborNoError) goto error;

    error:
    return err;
}
CborError isbipartite_decode_params(const CborValue *encoded_value, void **params) {
    CborError err = CborNoError;

    Graph *params_ptr = malloc(sizeof(Graph));
    *params = params_ptr;

    CborValue arrDecoder;

    err = cbor_value_get_array_length(encoded_value, &params_ptr->edge_count);
    if (err != CborNoError) goto error;
    params_ptr->edge_count = (params_ptr->edge_count - 1) / 2;
    params_ptr->edges = malloc(sizeof(GraphEdge) * params_ptr->edge_count);

    err = cbor_value_enter_container(encoded_value, &arrDecoder);
    if (err != CborNoError) goto error;
    err = cbor_value_get_uint64(&arrDecoder, &params_ptr->node_count);
    if (err != CborNoError) goto error;
    err = cbor_value_advance(&arrDecoder);
    if (err != CborNoError) goto error;
    for (size_t i = 0; i < params_ptr->edge_count; i++) {
        err = cbor_value_get_uint64(&arrDecoder, &params_ptr->edges[i].from);
        if (err != CborNoError) goto error;
        err = cbor_value_advance(&arrDecoder);
        if (err != CborNoError) goto error;
        err = cbor_value_get_uint64(&arrDecoder, &params_ptr->edges[i].to);
        if (err != CborNoError) goto error;
        err = cbor_value_advance(&arrDecoder);
        if (err != CborNoError) goto error;
    }

    error:
    return err;
}
CborError isbipartite_encode_value(CborEncoder *encoder, void *value) {
    return cbor_encode_boolean(encoder, *((bool *) value));
}
CborError isbipartite_decode_value(const CborValue *encoded_value, void **value) {
    *value = malloc(sizeof(bool));
    return cbor_value_get_boolean(encoded_value, *(bool **) value);
}
CborError isbipartite_encode_witness(CborEncoder *encoder, void *witness) {
    CborError err = CborNoError;

    IsbipartiteWitness* w = (IsbipartiteWitness*) witness;
    CborEncoder mapEncoder;
    err = cbor_encoder_create_map(encoder, &mapEncoder, 2);
    if (err != CborNoError) goto error;

    err = cbor_encode_text_stringz(&mapEncoder, "color");
    if (err != CborNoError) goto error;
    if (w->node_colors != NULL) {
        CborEncoder arrEncoder;
        err = cbor_encoder_create_array(&mapEncoder, &arrEncoder, w->count);
        if (err != CborNoError) goto error;
        for (size_t i = 0; i < w->count; i++) {
            err = cbor_encode_boolean(&arrEncoder, w->node_colors[i]);
            if (err != CborNoError) goto error;
        }
        err = cbor_encoder_close_container(&mapEncoder, &arrEncoder);
        if (err != CborNoError) goto error;
    } else {
        err = cbor_encode_null(&mapEncoder);
        if (err != CborNoError) goto error;
    }

    err = cbor_encode_text_stringz(&mapEncoder, "loop");
    if (err != CborNoError) goto error;
    if (w->loop_path != NULL) {
        CborEncoder arrEncoder;
        err = cbor_encoder_create_array(&mapEncoder, &arrEncoder, w->count);
        if (err != CborNoError) goto error;
        for (size_t i = 0; i < w->count; i++) {
            err = cbor_encode_uint(&arrEncoder, w->loop_path[i]);
            if (err != CborNoError) goto error;
        }
        err = cbor_encoder_close_container(&mapEncoder, &arrEncoder);
        if (err != CborNoError) goto error;
    } else {
        err = cbor_encode_null(&mapEncoder);
        if (err != CborNoError) goto error;
    }

    err = cbor_encoder_close_container(encoder, &mapEncoder);
    if (err != CborNoError) goto error;

    error:
    return err;
}
CborError isbipartite_decode_witness(const CborValue *encoded_value, void **witness) {
    CborError err = CborNoError;

    IsbipartiteWitness *witness_ptr = malloc(sizeof(IsbipartiteWitness));

    CborValue elementValue, arrValue;

    err = cbor_value_map_find_value(encoded_value, "color", &elementValue);
    if (err != CborNoError) goto error;
    if (cbor_value_is_null(&elementValue)) {
        witness_ptr->node_colors = NULL;
    } else {
        err = cbor_value_get_array_length(&elementValue, &witness_ptr->count);
        if (err != CborNoError) goto error;
        witness_ptr->node_colors = malloc(sizeof(bool) * witness_ptr->count);
        err = cbor_value_enter_container(&elementValue, &arrValue);
        if (err != CborNoError) goto error;
        for (size_t i = 0; i < witness_ptr->count; i++) {
            err = cbor_value_get_boolean(&arrValue, &witness_ptr->node_colors[i]);
            if (err != CborNoError) goto error;
            err = cbor_value_advance(&arrValue);
            if (err != CborNoError) goto error;
        }
        err = cbor_value_leave_container(&elementValue, &arrValue);
        if (err != CborNoError) goto error;
    }

    err = cbor_value_map_find_value(encoded_value, "loop", &elementValue);
    if (err != CborNoError) goto error;
    if (cbor_value_is_null(&elementValue)) {
        witness_ptr->loop_path = NULL;
        if (witness_ptr->node_colors == NULL) {
            err = CborErrorTooFewItems;
            goto error;
        }
    } else {
        if (witness_ptr->node_colors != NULL) {
            err = CborErrorTooManyItems;
            goto error;
        }
        err = cbor_value_get_array_length(&elementValue, &witness_ptr->count);
        if (err != CborNoError) goto error;
        witness_ptr->loop_path = malloc(sizeof(size_t) * witness_ptr->count);
        err = cbor_value_enter_container(&elementValue, &arrValue);
        if (err != CborNoError) goto error;
        for (size_t i = 0; i < witness_ptr->count; i++) {
            err = cbor_value_get_uint64(&arrValue, &witness_ptr->loop_path[i]);
            if (err != CborNoError) goto error;
            err = cbor_value_advance(&arrValue);
            if (err != CborNoError) goto error;
        }
        err = cbor_value_leave_container(&elementValue, &arrValue);
        if (err != CborNoError) goto error;
    }

    error:
    *witness = witness_ptr;
    return err;
}

void isbipartite_close_request(CeralRequest *req) {
    Graph *g = (Graph*) req->params;
    if (g->edges != NULL) { free(g->edges); g->edges = NULL; }
    free(g);
    free(req->params);
}
void isbipartite_close_response(CeralResponse *res) {
    free(res->value);
    IsbipartiteWitness *w = (IsbipartiteWitness *) res->witness;
    if (w->node_colors != NULL) { free(w->node_colors); w->node_colors = NULL; }
    if (w->loop_path != NULL) { free(w->loop_path); w->loop_path = NULL; }
    free(res->witness);
}

bool isbipartite_checker(CeralResponse *res) {
    Graph *g = (Graph*) res->req->params;
    bool value = *(bool*)res->value;
    IsbipartiteWitness *witness = (IsbipartiteWitness*)res->witness;
    return isbipartite_checker_raw(g, value, witness);
}

#if defined(CERAL_EXECUTOR) || defined(ISBIPARTITE_CERAL_EXECUTOR)
void isbipartite_executor(CeralResponse *context) {
    Graph *g = (Graph*) context->req->params;
    bool *value = malloc(sizeof(bool));
    IsbipartiteWitness *witness = new_isbipartite_witness(g);

    *value = isbipartite_executor_raw(g, witness);

    context->witness = witness;
    context->value = value;
}
#endif

CeralAlgorithm isbipartite_algorithm = {
    .algorithm_id = "ovgu.de/momarqua/ceral/isbipartite",

    .encode_params = isbipartite_encode_params,
    .decode_params = isbipartite_decode_params,
    .encode_value = isbipartite_encode_value,
    .decode_value = isbipartite_decode_value,
    .encode_witness = isbipartite_encode_witness,
    .decode_witness = isbipartite_decode_witness,

    .close_request = isbipartite_close_request,
    .close_response = isbipartite_close_response,

#if defined(CERAL_EXECUTOR) || defined(ISBIPARTITE_CERAL_EXECUTOR)
    .executor = isbipartite_executor,
#endif
    .checker = isbipartite_checker,
};
