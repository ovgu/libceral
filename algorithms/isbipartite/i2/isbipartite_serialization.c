#include <stdlib.h>
#include <ceral.h>
#include "isbipartite.h"

CborError isbipartite_encode_params(CborEncoder *encoder, void *params) {
    CborError err = CborNoError, err_test;

    Graph *g = (Graph*) params;
    CborEncoder arrEncoder;
    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_create_array(encoder, &arrEncoder, 1 + (g->edge_count * 2)));
    CERAL_HANDLE_CBOR_ERROR(cbor_encode_uint(&arrEncoder, g->node_count));
    for (size_t i = 0; i < g->edge_count; i++) {
        CERAL_HANDLE_CBOR_ERROR(cbor_encode_uint(&arrEncoder, g->edges[i].from));
        CERAL_HANDLE_CBOR_ERROR(cbor_encode_uint(&arrEncoder, g->edges[i].to));
    }
    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_close_container(encoder, &arrEncoder));

    error:
    return err;
}

CborError isbipartite_decode_params(CborValue *encoded_value, void **params) {
    CborError err = CborNoError, err_test;

    Graph *params_ptr = malloc(sizeof(Graph));
    *params = params_ptr;

    CborValue arrDecoder;

    CERAL_HANDLE_CBOR_ERROR(cbor_value_get_array_length(encoded_value, &params_ptr->edge_count));
    params_ptr->edge_count = (params_ptr->edge_count - 1) / 2;
    params_ptr->edges = malloc(sizeof(GraphEdge) * params_ptr->edge_count);

    CERAL_HANDLE_CBOR_ERROR(cbor_value_enter_container(encoded_value, &arrDecoder));
    CERAL_HANDLE_CBOR_ERROR(cbor_value_get_uint64(&arrDecoder, &params_ptr->node_count));
    CERAL_HANDLE_CBOR_ERROR(cbor_value_advance(&arrDecoder));
    for (size_t i = 0; i < params_ptr->edge_count; i++) {
        CERAL_HANDLE_CBOR_ERROR(cbor_value_get_uint64(&arrDecoder, &params_ptr->edges[i].from));
        CERAL_HANDLE_CBOR_ERROR(cbor_value_advance(&arrDecoder));
        CERAL_HANDLE_CBOR_ERROR(cbor_value_get_uint64(&arrDecoder, &params_ptr->edges[i].to));
        CERAL_HANDLE_CBOR_ERROR(cbor_value_advance(&arrDecoder));
    }

    error:
    return err;
}

CborError isbipartite_encode_value(CborEncoder *encoder, void *value) {
    return cbor_encode_boolean(encoder, *((bool *) value));
}

CborError isbipartite_decode_value(CborValue *encoded_value, void **value) {
    *value = malloc(sizeof(bool));
    return cbor_value_get_boolean(encoded_value, *(bool **) value);
}

CborError isbipartite_encode_witness(CborEncoder *encoder, void *witness) {
    CborError err = CborNoError, err_test;

    IsbipartiteWitness* w = (IsbipartiteWitness*) witness;
    CborEncoder mapEncoder;
    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_create_map(encoder, &mapEncoder, 2));

    CERAL_HANDLE_CBOR_ERROR(cbor_encode_text_stringz(&mapEncoder, "color"));
    if (w->node_colors != NULL) {
        CborEncoder arrEncoder;
        CERAL_HANDLE_CBOR_ERROR(cbor_encoder_create_array(&mapEncoder, &arrEncoder, w->count));
        for (size_t i = 0; i < w->count; i++) {
            CERAL_HANDLE_CBOR_ERROR(cbor_encode_boolean(&arrEncoder, w->node_colors[i]));
        }
        CERAL_HANDLE_CBOR_ERROR(cbor_encoder_close_container(&mapEncoder, &arrEncoder));
    } else {
        CERAL_HANDLE_CBOR_ERROR(cbor_encode_null(&mapEncoder));
    }

    CERAL_HANDLE_CBOR_ERROR(cbor_encode_text_stringz(&mapEncoder, "loop"));
    if (w->loop_path != NULL) {
        CborEncoder arrEncoder;
        CERAL_HANDLE_CBOR_ERROR(cbor_encoder_create_array(&mapEncoder, &arrEncoder, w->count));
        for (size_t i = 0; i < w->count; i++) {
            CERAL_HANDLE_CBOR_ERROR(cbor_encode_uint(&arrEncoder, w->loop_path[i]));
        }
        CERAL_HANDLE_CBOR_ERROR(cbor_encoder_close_container(&mapEncoder, &arrEncoder));
    } else {
        CERAL_HANDLE_CBOR_ERROR(cbor_encode_null(&mapEncoder));
    }

    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_close_container(encoder, &mapEncoder));

    error:
    return err;
}

CborError isbipartite_decode_witness(CborValue *encoded_value, void **witness) {
    IsbipartiteWitness *witness_ptr = malloc(sizeof(IsbipartiteWitness));

    CborError err = CborNoError, err_test;

    CborValue elementValue, arrValue;

    CERAL_HANDLE_CBOR_ERROR(cbor_value_map_find_value(encoded_value, "color", &elementValue));
    if (cbor_value_is_null(&elementValue)) {
        witness_ptr->node_colors = NULL;
    } else {
        CERAL_HANDLE_CBOR_ERROR(cbor_value_get_array_length(&elementValue, &witness_ptr->count));
        witness_ptr->node_colors = malloc(sizeof(bool) * witness_ptr->count);
        CERAL_HANDLE_CBOR_ERROR(cbor_value_enter_container(&elementValue, &arrValue));
        for (size_t i = 0; i < witness_ptr->count; i++) {
            CERAL_HANDLE_CBOR_ERROR(cbor_value_get_boolean(&arrValue, &witness_ptr->node_colors[i]));
            CERAL_HANDLE_CBOR_ERROR(cbor_value_advance(&arrValue));
        }
        CERAL_HANDLE_CBOR_ERROR(cbor_value_leave_container(&elementValue, &arrValue));
    }

    CERAL_HANDLE_CBOR_ERROR(cbor_value_map_find_value(encoded_value, "loop", &elementValue));
    if (cbor_value_is_null(&elementValue)) {
        witness_ptr->loop_path = NULL;
        if (witness_ptr->node_colors == NULL) {
            CERAL_HANDLE_CBOR_ERROR(CborErrorTooFewItems);
        }
    } else {
        if (witness_ptr->node_colors != NULL) {
            CERAL_HANDLE_CBOR_ERROR(CborErrorTooManyItems);
        }
        CERAL_HANDLE_CBOR_ERROR(cbor_value_get_array_length(&elementValue, &witness_ptr->count));
        witness_ptr->loop_path = malloc(sizeof(size_t) * witness_ptr->count);
        CERAL_HANDLE_CBOR_ERROR(cbor_value_enter_container(&elementValue, &arrValue));
        for (size_t i = 0; i < witness_ptr->count; i++) {
            CERAL_HANDLE_CBOR_ERROR(cbor_value_get_uint64(&arrValue, &witness_ptr->loop_path[i]));
            CERAL_HANDLE_CBOR_ERROR(cbor_value_advance(&arrValue));
        }
        CERAL_HANDLE_CBOR_ERROR(cbor_value_leave_container(&elementValue, &arrValue));
    }

    error:
    *witness = witness_ptr;
    return err;
}
