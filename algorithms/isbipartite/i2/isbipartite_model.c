#include <stdlib.h>
#include <ceral.h>
#include "isbipartite.h"

CeralRequest* new_isbipartite_request(size_t nodes, size_t edges) {
    Graph *params_ptr = malloc(sizeof(Graph));
    params_ptr->node_count = nodes;
    params_ptr->edge_count = edges;
    params_ptr->edges = malloc(sizeof(GraphEdge) * edges);
    return new_ceral_request(&isbipartite, params_ptr);
}

Graph* isbipartite_params(CeralRequest* req) {
    if (!ceral_algorithm_id_equals(req->algorithm_id, isbipartite.algorithm_id)) return 0;
    return (Graph*) req->params;
}
bool isbipartite_value(CeralResponse* res){
    if (!ceral_algorithm_id_equals(res->algorithm_id, isbipartite.algorithm_id)) return 0;
    return *(bool*)res->value;
}

IsbipartiteWitness* isbipartite_witness(CeralResponse* res) {
    if (!ceral_algorithm_id_equals(res->algorithm_id, isbipartite.algorithm_id)) return NULL;
    return (IsbipartiteWitness*)res->witness;
}
