#include <embUnit/embUnit.h>
#include <stdlib.h>
#include "../isbipartite.h"

CeralRequest *req;

static void setUp(void) {
    req = isbipartite_new_request(5, 3);
    Graph *g = isbipartite_params(req);
    g->edges[0].from = 0; g->edges[0].to = 1;
    g->edges[1].from = 0; g->edges[1].to = 2;
    g->edges[2].from = 3; g->edges[2].to = 2;
}

static void tearDown(void) {
    ceral_close_request(&isbipartite, req);
}

static void testInit(void) {
    // Check if the basic setup with isbipartite_new_request works
    TEST_ASSERT_EQUAL_INT(5, isbipartite_params(req)->node_count);
    TEST_ASSERT_EQUAL_INT(3, isbipartite_params(req)->edge_count);
    TEST_ASSERT_EQUAL_INT(3, isbipartite_params(req)->edges[2].from);
}

CeralResponse *resT, *resF;

static void testBipartiteness(void) {
    resT = ceral_new_response(&isbipartite, req);
    ceral_execute(&isbipartite, req, resT);
    TEST_ASSERT(isbipartite_value(resT));
    TEST_ASSERT(ceral_check(&isbipartite, req, resT));

    // TODO: new ID
    printf("---\n");

    Graph *g = isbipartite_params(req);
    g->edges[2].from = 2; g->edges[2].to = 1;
    resF = ceral_new_response(&isbipartite, req);
    ceral_execute(&isbipartite, req, resF);
    TEST_ASSERT(!isbipartite_value(resF));
    TEST_ASSERT(ceral_check(&isbipartite, req, resF));
}

static void testRequestEncoding(void) {
    uint8_t *b = malloc(CERAL_CBOR_DEFAULT_BUFFER_SIZE);
    uint8_t s = ceral_encode_request(&isbipartite, req, b, CERAL_CBOR_DEFAULT_BUFFER_SIZE);
    TEST_ASSERT(s > 0);

    CeralRequest *req_dec = ceral_new_request_from_cbor(&isbipartite, b, s);
    TEST_ASSERT_EQUAL_INT(req->request_id, req_dec->request_id);

    Graph* params = isbipartite_params(req);
    Graph* params_dec = isbipartite_params(req_dec);
    TEST_ASSERT_EQUAL_INT(params->edge_count, params_dec->edge_count);
    TEST_ASSERT_EQUAL_INT(params->node_count, params_dec->node_count);
    TEST_ASSERT(memcmp(params->edges, params_dec->edges, params->edge_count * sizeof(GraphEdge)) == 0);
}

static void testResponseEncoding(void) {
    uint8_t *b = malloc(CERAL_CBOR_DEFAULT_BUFFER_SIZE);
    uint8_t s = ceral_encode_response(&isbipartite, resT, b, CERAL_CBOR_DEFAULT_BUFFER_SIZE);
    TEST_ASSERT(s > 0);

    CeralResponse *resT_dec = ceral_new_response_from_cbor(&isbipartite, b, s);
    TEST_ASSERT_EQUAL_INT(resT->request_id, resT_dec->request_id);

    bool value = isbipartite_value(resT_dec);
    TEST_ASSERT(value);
    IsbipartiteWitness *witness = isbipartite_witness(resT_dec);
    TEST_ASSERT(witness->loop_path == NULL);
    TEST_ASSERT_EQUAL_INT(witness->count, isbipartite_witness(resT)->count);
    TEST_ASSERT(memcmp(isbipartite_witness(resT)->node_colors, witness->node_colors, sizeof(bool) * 5) == 0);

    // TODO: close

    b = malloc(CERAL_CBOR_DEFAULT_BUFFER_SIZE);
    s = ceral_encode_response(&isbipartite, resF, b, CERAL_CBOR_DEFAULT_BUFFER_SIZE);
    TEST_ASSERT(s > 0);

    CeralResponse *resF_dec = ceral_new_response_from_cbor(&isbipartite, b, s);
    TEST_ASSERT_EQUAL_INT(resF->request_id, resF_dec->request_id);

    value = isbipartite_value(resF_dec);
    TEST_ASSERT(!value);
    witness = isbipartite_witness(resF_dec);
    TEST_ASSERT(witness->node_colors == NULL);
    TEST_ASSERT_EQUAL_INT(witness->count, isbipartite_witness(resF)->count);
    TEST_ASSERT(memcmp(isbipartite_witness(resF)->loop_path, witness->loop_path, sizeof(size_t) * witness->count) == 0);
}

TestRef IsbipartiteTest_tests(void) {
    EMB_UNIT_TESTFIXTURES(fixtures) {
        new_TestFixture("testInit", testInit),
        new_TestFixture("testBipartiteness", testBipartiteness),
        new_TestFixture("testRequestEncoding", testRequestEncoding),
        new_TestFixture("testResponseEncoding", testResponseEncoding),
    };
    EMB_UNIT_TESTCALLER(IsbipartiteTest, "IsbipartiteTest", setUp, tearDown, fixtures);
    return (TestRef) &IsbipartiteTest;
}
