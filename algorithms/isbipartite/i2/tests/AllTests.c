#include <embUnit/embUnit.h>

#include "isbipartite-tests.c"
TestRef IsbipartiteTest_tests(void);

int main (int argc, const char* argv[])
{
    TestRunner_start();
    TestRunner_runTest(IsbipartiteTest_tests());
    TestRunner_end();
    return 0;
}
