#ifndef ISBIPARTITE_H
#define ISBIPARTITE_H

#include <ceral.h>
#include "../raw/isbipartite.c"

CeralAlgorithm isbipartite;

CeralRequest* new_isbipartite_request(size_t nodes, size_t edges);
Graph* isbipartite_params(CeralRequest* req);
bool isbipartite_value(CeralResponse* res);
IsbipartiteWitness* isbipartite_witness(CeralResponse* res);

#endif // ISBIPARTITE_H
