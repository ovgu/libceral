#ifndef ISBIPARTITE_NOCERAL
#include <ceral.h>
#endif
#include <stdlib.h>
#include <math.h>
#include "isbipartite.h"
#include "isbipartite_model.c"
#ifndef CERAL_DISABLE_CBOR
#include "isbipartite_serialization.c"
#endif

#ifndef ISBIPARTITE_NOCERAL
bool isbipartite_checker(CeralRequest *req, CeralResponse *res) {
    Graph *g = isbipartite_params(req);
    IsbipartiteWitness *witness = isbipartite_witness(res);
    bool value = isbipartite_value(res);
    return isbipartite_checker_raw(g, value, witness);
}
#endif

#ifndef ISBIPARTITE_NOEXEC
CeralError isbipartite_executor(CeralRequest *req, CeralResponse *res) {
    Graph *g = isbipartite_params(req);
    bool *value = malloc(sizeof(bool));
    IsbipartiteWitness *witness = new_isbipartite_witness(g);

    *value = isbipartite_executor_raw(g, witness);

    res->witness = witness;
    res->value = value;

    return CeralNoError;
}
#endif

void close_isbipartite_request(CeralRequest *req) {
    Graph *g = isbipartite_params(req);
    if (g->edges != NULL) { free(g->edges); g->edges = NULL; }
    free(g);
}
void close_isbipartite_response(CeralResponse *res) {
    free(res->value);
    IsbipartiteWitness *w = isbipartite_witness(res);
    if (w->node_colors != NULL) { free(w->node_colors); w->node_colors = NULL; }
    if (w->loop_path != NULL) { free(w->loop_path); w->loop_path = NULL; }
    free(res->witness);
}

CeralAlgorithm isbipartite = { CERAL_ALGORITHM(isbipartite, "\xD8\x70\x45\x83\xc2\x75\x1f\x03") };
