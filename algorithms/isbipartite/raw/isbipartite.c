#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "isbipartite.h"

bool isbipartite_checker_raw(Graph *g, bool v, IsbipartiteWitness *w) {
    if (v) {
        if (w->node_colors == NULL) return false;
        for (size_t e = 0; e < g->edge_count; e++) {
            if (w->node_colors[g->edges[e].from] == w->node_colors[g->edges[e].to]) {
                // Both nodes of an edge have the same color, so the proof failed.
                return false;
            }
        }
        return true;
    } else {
        if (w->count % 2 == 0 || w->count / 2 % 2 == 0) {
            // Loop is not odd-length, so the proof failed.
            //printf("wrong count %lu\n", w->count);
            return false;
        }
        // Verify that the loop follows edges of the graph
        for (size_t i = 0; i + 1 < w->count; i = i + 2) {
            //printf("from %lu via %lu to %lu\n", w->loop_path[i], w->loop_path[i+1], w->loop_path[i+2]);
            //printf("%d %lu %lu\n", GRAPH_EDGE_CONTAINS(g->edges[w->loop_path[i+1]], w->loop_path[i]), GRAPH_EDGE_OPPOSITE(g->edges[w->loop_path[i+1]], w->loop_path[i]), w->loop_path[i+2]);
            if (!GRAPH_EDGE_CONTAINS(g->edges[w->loop_path[i+1]], w->loop_path[i])) {
                // Edge doesn't match graph (doesn't contain edge), so the proof failed.
                return false;
            }
            if (i + 2 < w->count && GRAPH_EDGE_OPPOSITE(g->edges[w->loop_path[i+1]], w->loop_path[i]) != w->loop_path[i+2]) {
                // Edge doesn't match graph (other edge doesn't match), so the proof failed.
                return false;
            } else if (i + 2 >= w->count && GRAPH_EDGE_OPPOSITE(g->edges[w->loop_path[i+1]], w->loop_path[i]) != w->loop_path[0]) {
                // Edge doesn't match graph (loop is incomplete), so the proof failed.
                return false;
            }
        }
    }
    return true;
}

#ifndef ISBIPARTITE_NOEXEC
bool isbipartite_executor_raw(Graph *g, IsbipartiteWitness *witness) {
    bool value = true;

    bool finished[g->node_count];
    size_t colored_from[g->node_count];
    for (size_t i = 0; i < g->node_count; i++) {
        finished[i] = false;
        colored_from[i] = SIZE_MAX;
        witness->node_colors[i] = false;
    }
    // v is always the next unfinished node
    size_t v = 0;
    // Choose an arbitrary node r and color it red; also declare r unfinished.
    witness->node_colors[v] = false; // red
    // As long as there are unfinished nodes choose one of them, say v, and ...
    do {
        // ... declare it finished.
        finished[v] = true;
        // Go through all uncolored neighbors, ...
        for (size_t e = 0; e < g->edge_count; e++) {
            if (GRAPH_EDGE_CONTAINS(g->edges[e], v) && colored_from[GRAPH_EDGE_OPPOSITE(g->edges[e], v)] == SIZE_MAX) {
                // ... color them with the other color (i.e., the color different from v’s color), ...
                witness->node_colors[GRAPH_EDGE_OPPOSITE(g->edges[e], v)] = !witness->node_colors[v];
                // ... and add them to the set of unfinished nodes. ...
                finished[GRAPH_EDGE_OPPOSITE(g->edges[e], v)] = false;
                // ... Also, record that they got their color from v.
                colored_from[GRAPH_EDGE_OPPOSITE(g->edges[e], v)] = e;
                //printf("Colored %lu in %s due to an edge from %lu\n", GRAPH_EDGE_OPPOSITE(g->edges[e], v), witness->node_colors[GRAPH_EDGE_OPPOSITE(g->edges[e], v)] ? "blue" : "red", v);
            }
        }
        // Get next unfinished node.
        v = 0;
        while (v < g->node_count && finished[v]) v = v + 1;
    } while (v < g->node_count);
    // When step 2 terminates, all nodes are colored.

    // Iterate over all edges ...
    size_t e = 0;
    for (e = 0; e < g->edge_count; e++) {
        // ... and check that their endpoints have distinct colors.
        if (witness->node_colors[g->edges[e].from] == witness->node_colors[g->edges[e].to]) {
            value = false;
            // If not, let e = (u, v) be an edge whose endpoints have the same color.
            break;
        }
    }
    if (value) {
        // If so, output the two-coloring.
        witness->count = g->node_count;
        free(witness->loop_path);
        witness->loop_path = NULL;
    } else {
        // Follow the paths p u and p v of color-giving edges from u to r and from v to r;
        size_t pu[g->edge_count * 2];
        pu[0] = g->edges[e].from;
        pu[1] = e;
        size_t i = 2;
        while (i+2 < g->edge_count * 2 && GRAPH_EDGE_OPPOSITE(g->edges[pu[i-1]], pu[i-2]) != 0) {
            pu[i] = GRAPH_EDGE_OPPOSITE(g->edges[pu[i-1]], pu[i-2]);
            pu[i+1] = colored_from[pu[i]];
            i = i + 2;
        }
        pu[i] = GRAPH_EDGE_OPPOSITE(g->edges[pu[i-1]], pu[i-2]);
        i = i + 1;

        size_t pv[g->edge_count * 2];
        pv[0] = g->edges[e].to;
        pv[1] = colored_from[pv[0]];
        size_t j = 2;
        while (j+1 < g->edge_count * 2 && GRAPH_EDGE_OPPOSITE(g->edges[pv[j-1]], pv[j-2]) != 0) {
            pv[j] = GRAPH_EDGE_OPPOSITE(g->edges[pv[j-1]], pv[j-2]);
            pv[j+1] = colored_from[pv[j]];
            //printf("to %lu via %lu\n", pv[j], pv[j+1]);
            // TODO: something's wrong - this is never called, and the loop doesn't really match up
            j = j + 2;
        }

        // ... the paths together with e form an odd cycle. Why? Since u and v have the same color, the paths p u and p v either have both even length (if the color of u and v is the same as the color of r) or have both odd length (if the color of u and v differs from r’s color). Thus the combined length of p u and p v is even and hence the edge (u, v) closes an odd cycle.

        // Put paths together
        for (size_t ii = 0; ii < i; ii = ii + 1) {
            //printf("i %lu = %lu\n", ii, pu[ii]);
            witness->loop_path[ii] = pu[ii];
        }
        witness->count = i;
        //printf("%lu\n", pv[0]);
        if (j > 2 || pv[0] != 0) { // TODO: is that special-case needed?
            for (size_t jj = j; jj > 0; jj = jj - 1) {
                //printf("j %lu = %lu @ %lu\n", i + j - jj, pv[j - jj], j - jj);
                witness->loop_path[i + j - jj] = pv[j - jj];
            }
            witness->count = i + j;
        }

        // Output the odd-length path
        free(witness->node_colors);
        witness->node_colors = NULL;
    }

    return value;
}
#endif

IsbipartiteWitness* new_isbipartite_witness(Graph *graph) {
    IsbipartiteWitness *witness_ptr = malloc(sizeof(IsbipartiteWitness));
    witness_ptr->count = graph->edge_count * 4;
    witness_ptr->loop_path = malloc(sizeof(size_t) * witness_ptr->count);
    // I'm using bool here to make the code a bit easier to read - use a uint8_t as a bitfield for better memory efficiency.
    witness_ptr->node_colors = malloc(sizeof(bool) * graph->node_count);
    return witness_ptr;
}
