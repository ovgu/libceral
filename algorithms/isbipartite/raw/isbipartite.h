#ifndef CERTIFYING_ALGORITHMS_ISBIPARTITE_H
#define CERTIFYING_ALGORITHMS_ISBIPARTITE_H

#include <stdint.h>
#include <stdbool.h>

#define GRAPH_EDGE_CONTAINS(edge, node) (edge.from == node || edge.to == node)
#define GRAPH_EDGE_OPPOSITE(edge, node) (edge.from == node ? edge.to : edge.from)

typedef struct GraphEdge {
    uint64_t from;
    uint64_t to;
} GraphEdge;

typedef struct Graph {
    uint64_t node_count;
    size_t edge_count;
    GraphEdge *edges;
} Graph;

typedef struct IsbipartiteWitness {
    size_t count;
    bool *node_colors;
    uint64_t *loop_path; // node0 → edge → node → edge → ... → edge [→ node0]
} IsbipartiteWitness;

bool isbipartite_checker_raw(Graph *g, bool v, IsbipartiteWitness *w);
bool isbipartite_executor_raw(Graph *g, IsbipartiteWitness *witness);

IsbipartiteWitness* isbipartite_new_witness(Graph *graph);

#endif //CERTIFYING_ALGORITHMS_ISBIPARTITE_H
