package main

// #cgo LDFLAGS: -lceral -ltinycbor -lm
// #include <ceral.h>
// #include "digitsum_model.h"
// #include "digitsum_serialization.h"
// CeralAlgorithm digitsum;
import "C"

import (
    "fmt"
)

//export digitsum_executor
func digitsum_executor(req *C.CeralRequest, res *C.CeralResponse) C.CeralError {
    fmt.Printf("p %d\n", C.digitsum_params(req))
    res.witness = C.new_digitsum_witness()
    // TODO: calculate result
    res.value = C.new_digitsum_value(0)
    return C.CeralNoError
}

//export digitsum_checker
func digitsum_checker(req *C.CeralRequest, res *C.CeralResponse) C.bool {
    fmt.Printf("v %d\n", C.digitsum_value(res))
    fmt.Printf("w %d\n", C.digitsum_witness(res))
    // TODO: check result
    return false
}

func main() {}
