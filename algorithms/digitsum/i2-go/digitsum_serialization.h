#ifndef CERTIFYING_ALGORITHMS_DIGITSUM_SERIALIZATION_H
#define CERTIFYING_ALGORITHMS_DIGITSUM_SERIALIZATION_H

CborError digitsum_encode_params(CborEncoder *encoder, void *params);
CborError digitsum_decode_params(CborValue *encoded_value, void **params);
CborError digitsum_encode_value(CborEncoder *encoder, void *value);
CborError digitsum_decode_value(CborValue *encoded_value, void **value);
CborError digitsum_encode_witness(CborEncoder *encoder, void *witness);
CborError digitsum_decode_witness(CborValue *encoded_value, void **witness);

#endif //CERTIFYING_ALGORITHMS_DIGITSUM_SERIALIZATION_H
