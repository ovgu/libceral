#include <stdlib.h>
#include <ceral.h>
#include "libdigitsum.h"
#include "digitsum_model.h"

CeralRequest *new_digitsum_request(int params) {
    int *params_ptr = malloc(sizeof(params));
    *params_ptr = params;
    return new_ceral_request(&digitsum, params_ptr);
}

int digitsum_params(CeralRequest *req) {
    if (!ceral_algorithm_id_equals(req->algorithm_id, digitsum.algorithm_id)) return 0;
    return *(int *) req->params;
}

int digitsum_value(CeralResponse *res) {
    if (!ceral_algorithm_id_equals(res->algorithm_id, digitsum.algorithm_id)) return 0;
    return *(int *) res->value;
}

uint8_t *digitsum_witness(CeralResponse *res) {
    if (!ceral_algorithm_id_equals(res->algorithm_id, digitsum.algorithm_id)) return NULL;
    return (uint8_t *) res->witness;
}

void *new_digitsum_value(int value) {
    return malloc(sizeof(int));
}
void *new_digitsum_witness() {
    return malloc(sizeof(char) * 11);
}

void close_digitsum_request(CeralRequest *req) {
    free(req->params);
}
void close_digitsum_response(CeralResponse *res) {
    free(res->value);
    free(res->witness);
}