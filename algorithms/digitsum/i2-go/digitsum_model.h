#ifndef CERTIFYING_ALGORITHMS_DIGITSUM_H
#define CERTIFYING_ALGORITHMS_DIGITSUM_H

CeralAlgorithm digitsum;

CeralRequest *new_digitsum_request(int params);
int digitsum_params(CeralRequest *req);
int digitsum_value(CeralResponse *res);
uint8_t *digitsum_witness(CeralResponse *res);

void close_digitsum_request(CeralRequest *req);
void close_digitsum_response(CeralResponse *res);

void *new_digitsum_value(int value);
void *new_digitsum_witness();

#endif //CERTIFYING_ALGORITHMS_DIGITSUM_H
