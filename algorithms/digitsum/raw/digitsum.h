#ifndef CERTIFYING_ALGORITHMS_DIGITSUM_H
#define CERTIFYING_ALGORITHMS_DIGITSUM_H

#include <stdint.h>
#include <stdbool.h>

bool digitsum_checker_raw(int params, int digitsum, uint8_t* witness);
void digitsum_executor_raw(int params, int* value, uint8_t *witness);

#endif //CERTIFYING_ALGORITHMS_DIGITSUM_H
