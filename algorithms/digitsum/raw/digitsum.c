#include <stdio.h>
#include <math.h>
#include "digitsum.h"

bool digitsum_checker_raw(int params, int digitsum, uint8_t* witness) {
    int total = 0;
    for (int i = 0; i < 10; i++) {
        digitsum = digitsum - witness[i];
        total = total + witness[i] * (int) pow(10, 9 - i);
    }
    return digitsum == 0 && total == params;
}

void digitsum_executor_raw(int params, int* value, uint8_t *witness) {
    *value = 0;

    sprintf((char*) witness, "%010d", params);
    for (int i = 0; i < 10 && witness[i]; i++) {
        if (witness[i] >= 48 && witness[i] <= 57) {
            witness[i] = witness[i] - 48;
            *value = *value + witness[i];
        }
    }

    // intended error case, occurs e.g. with digit_sum(99996)
    if (*value == 42) *value = 41;
}
