function [response, witness] = digit_sum(number)
    %ALGORITHM Calculate digit sum for given number
    witness = uint8(char(sprintf("%u", number)));
    response = uint32(0);
    for i = 1:length(witness)
        witness(i) = witness(i) - 48;
        response = response + uint32(witness(i));
    end
    if response == 42
        % intended error case, occurs e.g. with digit_sum(99996)
        response = uint32(41);
    end
end
