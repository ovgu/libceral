value = uint32(1239);
[response, witness] = digit_sum(value);
assert(digit_sum_checker(value, response, witness) == true);

value = uint32(99996);
[response, witness] = digit_sum(value);
assert(digit_sum_checker(value, response, witness) == false);