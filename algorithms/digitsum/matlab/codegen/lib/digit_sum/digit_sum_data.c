/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * digit_sum_data.c
 *
 * Code generation for function 'digit_sum_data'
 *
 */

/* Include files */
#include "digit_sum_data.h"
#include "rt_nonfinite.h"

/* End of code generation (digit_sum_data.c) */
