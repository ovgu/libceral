/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * digit_sum.c
 *
 * Code generation for function 'digit_sum'
 *
 */

/* Include files */
#include "digit_sum.h"
#include "digit_sum_emxutil.h"
#include "digit_sum_types.h"
#include "rt_nonfinite.h"
#include <stddef.h>
#include <stdio.h>

/* Function Definitions */
void digit_sum(unsigned int number, unsigned int *response,
               emxArray_uint8_T *witness)
{
  emxArray_char_T *charStr;
  int b_i;
  int i;
  int nbytes;
  unsigned int qY;
  char *charStr_data;
  unsigned char *witness_data;
  emxInit_char_T(&charStr);
  /* ALGORITHM Calculate digit sum for given number */
  nbytes = snprintf(NULL, 0, "%u", number);
  i = charStr->size[0] * charStr->size[1];
  charStr->size[0] = 1;
  charStr->size[1] = nbytes + 1;
  emxEnsureCapacity_char_T(charStr, i);
  charStr_data = charStr->data;
  snprintf(&charStr_data[0], (size_t)(nbytes + 1), "%u", number);
  i = charStr->size[0] * charStr->size[1];
  if (1 > nbytes) {
    charStr->size[1] = 0;
  } else {
    charStr->size[1] = nbytes;
  }
  emxEnsureCapacity_char_T(charStr, i);
  charStr_data = charStr->data;
  i = witness->size[0] * witness->size[1];
  witness->size[0] = 1;
  witness->size[1] = charStr->size[1];
  emxEnsureCapacity_uint8_T(witness, i);
  witness_data = witness->data;
  nbytes = charStr->size[1];
  for (i = 0; i < nbytes; i++) {
    witness_data[i] = (unsigned char)charStr_data[i];
  }
  emxFree_char_T(&charStr);
  *response = 0U;
  i = witness->size[1];
  for (b_i = 0; b_i < i; b_i++) {
    nbytes = witness_data[b_i];
    qY = nbytes - 48U;
    if (nbytes - 48U > (unsigned int)nbytes) {
      qY = 0U;
    }
    witness_data[b_i] = (unsigned char)qY;
    qY = *response + (unsigned char)qY;
    if (qY < *response) {
      qY = MAX_uint32_T;
    }
    *response = qY;
  }
  if (*response == 42U) {
    /*  intended error case, occurs e.g. with digit_sum(99996) */
    *response = 41U;
  }
}

/* End of code generation (digit_sum.c) */
