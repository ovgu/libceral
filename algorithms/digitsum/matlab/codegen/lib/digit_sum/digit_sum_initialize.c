/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * digit_sum_initialize.c
 *
 * Code generation for function 'digit_sum_initialize'
 *
 */

/* Include files */
#include "digit_sum_initialize.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void digit_sum_initialize(void)
{
}

/* End of code generation (digit_sum_initialize.c) */
