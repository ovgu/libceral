/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * digit_sum_initialize.h
 *
 * Code generation for function 'digit_sum_initialize'
 *
 */

#ifndef DIGIT_SUM_INITIALIZE_H
#define DIGIT_SUM_INITIALIZE_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern void digit_sum_initialize(void);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (digit_sum_initialize.h) */
