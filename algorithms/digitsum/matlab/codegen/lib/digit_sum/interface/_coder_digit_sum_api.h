/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_digit_sum_api.h
 *
 * Code generation for function 'digit_sum'
 *
 */

#ifndef _CODER_DIGIT_SUM_API_H
#define _CODER_DIGIT_SUM_API_H

/* Include files */
#include "emlrt.h"
#include "tmwtypes.h"
#include <string.h>

/* Type Definitions */
#ifndef struct_emxArray_uint8_T
#define struct_emxArray_uint8_T
struct emxArray_uint8_T {
  uint8_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};
#endif /* struct_emxArray_uint8_T */
#ifndef typedef_emxArray_uint8_T
#define typedef_emxArray_uint8_T
typedef struct emxArray_uint8_T emxArray_uint8_T;
#endif /* typedef_emxArray_uint8_T */

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void digit_sum(uint32_T number, uint32_T *response, emxArray_uint8_T *witness);

void digit_sum_api(const mxArray *prhs, int32_T nlhs, const mxArray *plhs[2]);

void digit_sum_atexit(void);

boolean_T digit_sum_checker(uint32_T request, uint32_T response,
                            emxArray_uint8_T *witness);

void digit_sum_checker_api(const mxArray *const prhs[3], const mxArray **plhs);

void digit_sum_initialize(void);

void digit_sum_terminate(void);

void digit_sum_xil_shutdown(void);

void digit_sum_xil_terminate(void);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (_coder_digit_sum_api.h) */
