/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * digit_sum_checker.c
 *
 * Code generation for function 'digit_sum_checker'
 *
 */

/* Include files */
#include "digit_sum_checker.h"
#include "digit_sum_types.h"
#include "rt_nonfinite.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Declarations */
static double rt_powd_snf(double u0, double u1);

static double rt_roundd_snf(double u);

/* Function Definitions */
static double rt_powd_snf(double u0, double u1)
{
  double d;
  double d1;
  double y;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = rtNaN;
  } else {
    d = fabs(u0);
    d1 = fabs(u1);
    if (rtIsInf(u1)) {
      if (d == 1.0) {
        y = 1.0;
      } else if (d > 1.0) {
        if (u1 > 0.0) {
          y = rtInf;
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = rtInf;
      }
    } else if (d1 == 0.0) {
      y = 1.0;
    } else if (d1 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > floor(u1))) {
      y = rtNaN;
    } else {
      y = pow(u0, u1);
    }
  }
  return y;
}

static double rt_roundd_snf(double u)
{
  double y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }
  return y;
}

bool digit_sum_checker(unsigned int request, unsigned int response,
                       const emxArray_uint8_T *witness)
{
  double d;
  int exitg1;
  int i;
  unsigned int qY;
  unsigned int request_from_witness;
  unsigned int response_from_witness;
  const unsigned char *witness_data;
  bool valid;
  witness_data = witness->data;
  /* DIGIT_SUM_CHECKER Checks if the response of the digit_sum method is valid
   */
  response_from_witness = 0U;
  request_from_witness = 0U;
  i = 0;
  do {
    exitg1 = 0;
    if (i <= witness->size[1] - 1) {
      qY = response_from_witness + witness_data[i];
      if (qY < response_from_witness) {
        qY = MAX_uint32_T;
      }
      response_from_witness = qY;
      d = rt_roundd_snf((double)witness_data[i] *
                        rt_powd_snf(10.0, (witness->size[1] - i) - 1));
      if (d < 4.294967296E+9) {
        qY = (unsigned int)d;
      } else if (d >= 4.294967296E+9) {
        qY = MAX_uint32_T;
      } else {
        qY = 0U;
      }
      qY += request_from_witness;
      if (qY < request_from_witness) {
        qY = MAX_uint32_T;
      }
      request_from_witness = qY;
      d = rt_roundd_snf(rt_powd_snf(10.0, (witness->size[1] - i) - 1));
      if (d < 4.294967296E+9) {
        qY = (unsigned int)d;
      } else {
        qY = MAX_uint32_T;
      }
      if (qY == 0U) {
        qY = MAX_uint32_T;
      } else {
        qY = request / qY;
      }
      qY -= qY / 10U * 10U;
      if (qY > 255U) {
        qY = 255U;
      }
      if ((int)qY != witness_data[i]) {
        valid = false;
        exitg1 = 1;
      } else {
        i++;
      }
    } else {
      if ((response == response_from_witness) &&
          (request == request_from_witness)) {
        valid = true;
      } else {
        valid = false;
      }
      exitg1 = 1;
    }
  } while (exitg1 == 0);
  return valid;
}

/* End of code generation (digit_sum_checker.c) */
