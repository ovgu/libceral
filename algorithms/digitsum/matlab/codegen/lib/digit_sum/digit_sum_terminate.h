/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * digit_sum_terminate.h
 *
 * Code generation for function 'digit_sum_terminate'
 *
 */

#ifndef DIGIT_SUM_TERMINATE_H
#define DIGIT_SUM_TERMINATE_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern void digit_sum_terminate(void);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (digit_sum_terminate.h) */
