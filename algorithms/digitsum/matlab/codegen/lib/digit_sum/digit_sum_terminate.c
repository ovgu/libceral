/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * digit_sum_terminate.c
 *
 * Code generation for function 'digit_sum_terminate'
 *
 */

/* Include files */
#include "digit_sum_terminate.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void digit_sum_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (digit_sum_terminate.c) */
