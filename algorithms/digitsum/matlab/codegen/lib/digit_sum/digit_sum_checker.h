/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * digit_sum_checker.h
 *
 * Code generation for function 'digit_sum_checker'
 *
 */

#ifndef DIGIT_SUM_CHECKER_H
#define DIGIT_SUM_CHECKER_H

/* Include files */
#include "digit_sum_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern bool digit_sum_checker(unsigned int request, unsigned int response,
                              const emxArray_uint8_T *witness);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (digit_sum_checker.h) */
