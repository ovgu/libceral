function [valid] = digit_sum_checker(request, response, witness)
    %DIGIT_SUM_CHECKER Checks if the response of the digit_sum method is valid
    response_from_witness = uint32(0);
    request_from_witness = uint32(0);
    for i = 1:length(witness)
        response_from_witness = response_from_witness + uint32(witness(i));
        request_from_witness = request_from_witness + uint32(witness(i)) * 10 ^ (length(witness) - i);
        digit = uint8(floor(mod(idivide(request, uint32(10 ^ (length(witness) - i))), uint32(10))));
        if digit ~= witness(i)
            valid = false;
            return;
        end
    end
    valid = (response == response_from_witness) && (request == request_from_witness);
end

