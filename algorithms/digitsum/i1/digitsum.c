#include <stdlib.h>
#include <ceral.h>
#include <math.h>

CborError digitsum_encode_params(CborEncoder *encoder, void *params) {
    return cbor_encode_int(encoder, *((int *) params));
}
CborError digitsum_decode_params(const CborValue *encoded_value, void **params) {
    *params = malloc(sizeof(int));
    return cbor_value_get_int(encoded_value, *(int **)params);
}
CborError digitsum_encode_value(CborEncoder *encoder, void *value) {
    return cbor_encode_int(encoder, *((int *) value));
}
CborError digitsum_decode_value(const CborValue *encoded_value, void **value) {
    *value = malloc(sizeof(int));
    return cbor_value_get_int(encoded_value, *(int **)value);
}
CborError digitsum_encode_witness(CborEncoder *encoder, void *witness) {
    return cbor_encode_byte_string(encoder, (uint8_t *) witness, 11);
}
CborError digitsum_decode_witness(const CborValue *encoded_value, void **witness) {
    *witness = malloc(11 * sizeof(uint8_t));
    size_t size = 11;
    CborValue noop;
    return cbor_value_copy_byte_string(encoded_value, *(uint8_t **)witness, &size, &noop);
}

void digitsum_close_request(CeralRequest *req) {
    free(req->params);
}
void digitsum_close_response(CeralResponse *res) {
    free(res->value);
    free(res->witness);
}

bool digitsum_checker(CeralResponse *res) {
    char *witness = res->witness;
    int digitsum = *((int *) res->value);
    int total = 0;
    for (int i = 0; i < 10; i++) {
        digitsum = digitsum - witness[i];
        total = total + witness[i] * (int) pow(10, 9 - i);
    }
    return digitsum == 0 && total == *((int *) res->req->params);
}

#if defined(CERAL_EXECUTOR) || defined(DIGITSUM_CERAL_EXECUTOR)
void digitsum_executor(CeralResponse *context) {
    int params = *((int *) context->req->params);
    int *digitsum = malloc(sizeof(int));
    *digitsum = 0;

    char *buffer = malloc(11 * sizeof(uint8_t));
    sprintf(buffer, "%010d", params);
    for (int i = 0; i < 10 && buffer[i]; i++) {
        if (buffer[i] >= 48 && buffer[i] <= 57) {
            buffer[i] = buffer[i] - 48;
            *digitsum = *digitsum + buffer[i];
        }
    }

    // intended error case, occurs e.g. with digit_sum(99996)
    if (*digitsum == 42) *digitsum = 41;

    context->witness = buffer;
    context->value = digitsum;
}
#endif

CeralAlgorithm digitsum_algorithm = {
    .algorithm_id = "ovgu.de/momarqua/ceral/digitsum",

    .encode_params = digitsum_encode_params,
    .decode_params = digitsum_decode_params,
    .encode_value = digitsum_encode_value,
    .decode_value = digitsum_decode_value,
    .encode_witness = digitsum_encode_witness,
    .decode_witness = digitsum_decode_witness,

    .close_request = digitsum_close_request,
    .close_response = digitsum_close_response,

#if defined(CERAL_EXECUTOR) || defined(DIGITSUM_CERAL_EXECUTOR)
    .executor = digitsum_executor,
#endif
    .checker = digitsum_checker,
};
