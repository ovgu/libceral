#include <ceral.h>
#include <stdlib.h>
#include <math.h>
#include "digitsum.h"
#include "digitsum_model.c"
#ifndef CERAL_DISABLE_CBOR
#include "digitsum_serialization.c"
#endif

bool digitsum_checker(CeralRequest *req, CeralResponse *res) {
    uint8_t* witness = res->witness;
    int digitsum = *((int *) res->value);
    int total = 0;
    for (int i = 0; i < 10; i++) {
        digitsum = digitsum - witness[i];
        total = total + witness[i] * (int) pow(10, 9 - i);
    }
    return digitsum == 0 && total == *((int *) req->params);
}

CeralError digitsum_executor(CeralRequest *req, CeralResponse *res) {
#ifndef DIGITSUM_NOEXEC
    int params = *((int *) req->params);
    int *digitsum = malloc(sizeof(int));
    *digitsum = 0;

    char *buffer = malloc(11 * sizeof(uint8_t));
    sprintf(buffer, "%010d", params);
    for (int i = 0; i < 10 && buffer[i]; i++) {
        if (buffer[i] >= 48 && buffer[i] <= 57) {
            buffer[i] = buffer[i] - 48;
            *digitsum = *digitsum + buffer[i];
        }
    }

    // intended error case, occurs e.g. with digit_sum(99996)
    if (*digitsum == 42) *digitsum = 41;

    res->witness = buffer;
    res->value = digitsum;
    return CeralNoError;
#else
    return CeralMissingAlgorithmError;
#endif
}

void close_digitsum_request(CeralRequest *req) {
    free(req->params);
}
void close_digitsum_response(CeralResponse *res) {
    free(res->value);
    free(res->witness);
}

CeralAlgorithm digitsum = { CERAL_ALGORITHM(digitsum, "\xD8\x70\x45\x83\xc2\x75\x1f\x00") };
