#ifndef DIGITSUM_H
#define DIGITSUM_H

#include <stdlib.h>
#include <ceral.h>

CeralAlgorithm digitsum;

CeralRequest* new_digitsum_request(int params);
int digitsum_params(CeralRequest* req);
int digitsum_value(CeralResponse* res);
uint8_t* digitsum_witness(CeralResponse* res);

#endif // DIGITSUM_H
