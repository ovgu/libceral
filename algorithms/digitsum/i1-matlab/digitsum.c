#include <stdlib.h>
#include <ceral.h>
#include <math.h>
#include "../matlab/codegen/lib/digit_sum/digit_sum.h"
#include "../matlab/codegen/lib/digit_sum/digit_sum_checker.h"
#include "../matlab/codegen/lib/digit_sum/digit_sum_emxAPI.h"
#include "../matlab/codegen/lib/digit_sum/digit_sum_terminate.h"
#include "../matlab/codegen/lib/digit_sum/digit_sum_types.h"

CborError digitsum_encode_params(CborEncoder *encoder, void *params) {
    return cbor_encode_int(encoder, *((int *) params));
}
CborError digitsum_decode_params(const CborValue *encoded_value, void **params) {
    *params = malloc(sizeof(int));
    return cbor_value_get_int(encoded_value, *(int **)params);
}
CborError digitsum_encode_value(CborEncoder *encoder, void *value) {
    return cbor_encode_int(encoder, *((int *) value));
}
CborError digitsum_decode_value(const CborValue *encoded_value, void **value) {
    *value = malloc(sizeof(int));
    return cbor_value_get_int(encoded_value, *(int **)value);
}
CborError digitsum_encode_witness(CborEncoder *encoder, void *witness) {
    return cbor_encode_byte_string(encoder, (uint8_t *) witness, 11);
}
CborError digitsum_decode_witness(const CborValue *encoded_value, void **witness) {
    *witness = malloc(11 * sizeof(uint8_t));
    size_t size = 11;
    CborValue noop;
    return cbor_value_copy_byte_string(encoded_value, *(uint8_t **)witness, &size, &noop);
}

void digitsum_close_request(CeralRequest *req) {
    free(req->params);
}
void digitsum_close_response(CeralResponse *res) {
    free(res->value);
    free(res->witness);
}

bool digitsum_checker(CeralResponse *res) {
    int request = *((int *) res->req->params);
    int digitsum = *((int *) res->value);

    //char *witness = res->witness;
    emxArray_uint8_T *witnessML;
    emxInitArray_uint8_T(&witnessML, 2);
    //witnessML->data[0] = witness[0]; // TODO: add a for loop to use real data

    bool valid = digit_sum_checker(request, (unsigned int) digitsum, witnessML);
    emxDestroyArray_uint8_T(witnessML);
    digit_sum_terminate();

    return valid;
}

#if defined(CERAL_EXECUTOR) || defined(DIGITSUM_CERAL_EXECUTOR)
void digitsum_executor(CeralResponse *context) {
    int params = *((int *) context->req->params);
    context->value = malloc(sizeof(int));
    char *buffer = malloc(11 * sizeof(uint8_t));

    emxArray_uint8_T *witness;
    unsigned int request = (unsigned int) params;
    emxInitArray_uint8_T(&witness, 2);
    digit_sum(request, (unsigned int*) context->value, witness);

    for (size_t i = 0; i < 11; i++) buffer[i] = witness->data[i];
    context->witness = buffer;
}
#endif

CeralAlgorithm digitsum_algorithm = {
        .algorithm_id = "ovgu.de/momarqua/ceral/digitsum",

        .encode_params = digitsum_encode_params,
        .decode_params = digitsum_decode_params,
        .encode_value = digitsum_encode_value,
        .decode_value = digitsum_decode_value,
        .encode_witness = digitsum_encode_witness,
        .decode_witness = digitsum_decode_witness,

        .close_request = digitsum_close_request,
        .close_response = digitsum_close_response,

#if defined(CERAL_EXECUTOR) || defined(DIGITSUM_CERAL_EXECUTOR)
        .executor = digitsum_executor,
#endif
        .checker = digitsum_checker,
};
