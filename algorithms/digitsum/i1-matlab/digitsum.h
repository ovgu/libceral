#ifndef CERTIFYING_ALGORITHMS_DIGITSUM_H
#define CERTIFYING_ALGORITHMS_DIGITSUM_H

#include <stdlib.h>
#include <ceral.h>

CeralAlgorithm digitsum;

void digitsum_close_request(CeralRequest *req);
CeralAlgorithm digitsum_algorithm;

#endif //CERTIFYING_ALGORITHMS_DIGITSUM_H
