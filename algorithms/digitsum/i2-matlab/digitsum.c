#include <ceral.h>
#include <stdlib.h>
#include <math.h>
#include "digitsum.h"
#include "../i2/digitsum_model.c"
#ifndef CERAL_DISABLE_CBOR
#include "../i2/digitsum_serialization.c"
#endif
#include "../matlab/codegen/lib/digit_sum/digit_sum.h"
#include "../matlab/codegen/lib/digit_sum/digit_sum_checker.h"
#include "../matlab/codegen/lib/digit_sum/digit_sum_emxAPI.h"
#include "../matlab/codegen/lib/digit_sum/digit_sum_terminate.h"
#include "../matlab/codegen/lib/digit_sum/digit_sum_types.h"

bool digitsum_checker(CeralRequest *req, CeralResponse *res) {
    int request = *((int *) req->params);
    int digitsum = *((int *) res->value);

    //char *witness = res->witness;
    emxArray_uint8_T *witnessML;
    emxInitArray_uint8_T(&witnessML, 2);
    //witnessML->data[0] = witness[0]; // TODO: add a for loop to use real data

    bool valid = digit_sum_checker(request, (unsigned int) digitsum, witnessML);
    emxDestroyArray_uint8_T(witnessML);
    //digit_sum_terminate();
    return valid;
}

CeralError digitsum_executor(CeralRequest *req, CeralResponse *res) {
#ifndef DIGITSUM_NOEXEC
    int params = *((int *) req->params);
    res->value = malloc(sizeof(int));
    char *buffer = malloc(11 * sizeof(uint8_t));

    emxArray_uint8_T *witness;
    unsigned int request = (unsigned int) params;
    emxInitArray_uint8_T(&witness, 2);
    digit_sum(request, (unsigned int*) res->value, witness);

    for (size_t i = 0; i < 11; i++) buffer[i] = witness->data[i];
    res->witness = buffer;
    return CeralNoError;
#else
    return CeralMissingAlgorithmError;
#endif
}

void close_digitsum_request(CeralRequest *req) {
    free(req->params);
}
void close_digitsum_response(CeralResponse *res) {
    free(res->value);
    free(res->witness);
}

CeralAlgorithm digitsum = { CERAL_ALGORITHM(digitsum, "\xD8\x70\x45\x83\xc2\x75\x1f\x00") };
