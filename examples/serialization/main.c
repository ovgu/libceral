#include <ceral.h>
#include "../../../Algorithms/digitsum/i2/digitsum.h"

int main(void) {
    // Create a request
    CeralRequest *req = new_digitsum_request(71);

    // CALLER: encode request
    uint8_t *req_enc = malloc(5);
    size_t req_len = ceral_encode_request(&digitsum, req, req_enc, 5);
    // Print request CBOR
    printf("Request as CBOR:  ");
    for (int i = 0; i < req_len; i++) printf("%02x", req_enc[i]);
    printf("\n");

    // Here would be the place to hand the buffer over to the executor node

    // EXECUTOR: deserialize the request
    CeralRequest *exec_req = new_ceral_request_from_cbor(&digitsum, req_enc, req_len);

    // Execute the algorithm
    CeralResponse *exec_res = new_ceral_response(&digitsum, exec_req);
    ceral_execute(&digitsum, exec_req, exec_res);
    if (exec_res->error != CeralNoError) {
        printf("Algorithm Error: %d\n", exec_res->error);
        // can we somehow print the OID as a string for debugging?
        return 1;
    }

    // EXECUTOR: serialize the response
    uint8_t *res_enc = malloc(CERAL_CBOR_DEFAULT_BUFFER_SIZE);
    size_t res_len = ceral_encode_response(&digitsum, exec_res, res_enc, CERAL_CBOR_DEFAULT_BUFFER_SIZE);
    // Print response CBOR
    printf("Response as CBOR: ");
    for (int i = 0; i < res_len; i++) printf("%02x", res_enc[i]);
    printf("\n");

    // Here would be the place to hand the buffer over back to the caller node

    CeralResponse *res = new_ceral_response_from_cbor(&digitsum, res_enc, res_len);
    bool chk = ceral_check(&digitsum, req, res);

    // Print output
    printf("Result for request %d = %d\n", digitsum_params(req), digitsum_value(res));
    printf("Result is %s; witness: ", chk ? "correct" : "NOT correct");
    uint8_t* w = digitsum_witness(res);
    for (int i = 0; i < 10; i++) printf("%d ", w[i]);
    printf("\n");
}
