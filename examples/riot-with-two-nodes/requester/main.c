#include "../common.c"

CeralRequest *req = NULL;

uint64_t t0_start, t1_serialized, t2_sent, t3_received, t4_deserialized, t5_checked;

void receive_data(uint8_t data_size) {
    t3_received = xtimer_now_usec64();
    CeralResponse *res = new_ceral_response_from_cbor(&digitsum, buffer, data_size);
    t4_deserialized = xtimer_now_usec64();

    // It's a response! Check it and print if it was successful.
    bool chk = ceral_check(&digitsum, req, res);
    t5_checked = xtimer_now_usec64();
    DEBUG("RES [%llu] Received response for own request: digit_sum(%d) = %d %s\n",
             res->request_id,
             *((int *) req->params),
             *((int *) res->value),
             chk ? "(valid)" : "(invalid)"
    );

    close_ceral_request(&digitsum, req);
    close_ceral_response(&digitsum, res);
    req = NULL;

    printf("Requester Timings: %llu, %llu, %llu, %llu, %llu, %llu\n", t0_start, t1_serialized, t2_sent, t3_received, t4_deserialized, t5_checked);
}

int main(void) {
    int uart_state = uart_init(COM_PORT, STDIO_UART_BAUDRATE, receive_byte, 0);
    if (uart_state != 0) {
        DEBUG("UART setup failed with state %d.\n", uart_state);
    } else {
        DEBUG("All set up.\n");
    }

    while (true) {

        DEBUG("Creating request...\n");
        req = new_digitsum_request(rand());
        DEBUG("REQ [%llu] Sending request to other node: %d\n",
                 req->request_id,
                 digitsum_params(req)
        );

        DEBUG("Serializing request...\n");
        t0_start = xtimer_now_usec64();
        uint8_t *req_enc = malloc(CERAL_CBOR_DEFAULT_BUFFER_SIZE);
        size_t req_len = ceral_encode_request(&digitsum, req, req_enc, CERAL_CBOR_DEFAULT_BUFFER_SIZE);
        t1_serialized = xtimer_now_usec64();

        // Send the request through UART
        DEBUG("Sending %u bytes...\n", req_len);
        uart_write_size(COM_PORT, req_len);
        uart_write(COM_PORT, req_enc, req_len);
        t2_sent = xtimer_now_usec64();

        DEBUG("Waiting until response...\n");
        while (req != NULL) {
            xtimer_msleep(10);
        }

        DEBUG("Waiting random amount of time before sending next request...\n");
        xtimer_msleep(1000 + (rand() % 10000));

    }
}
