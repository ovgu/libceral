#include "../common.c"

uint64_t t0_received, t1_deserialized, t2_executed, t3_serialized, t4_sent;

void receive_data(uint8_t data_size) {
    t0_received = xtimer_now_usec64();
    CeralRequest *req = new_ceral_request_from_cbor(&digitsum, buffer, data_size);
    CeralResponse *res = new_ceral_response(&digitsum, req);
    t1_deserialized = xtimer_now_usec64();

    // It's a response! Check it and print if it was successful.
    ceral_execute(&digitsum, req, res);
    t2_executed = xtimer_now_usec64();
    DEBUG("RES [%llu] Calculated response: digit_sum(%d) = %d\n",
             res->request_id,
             *((int *) req->params),
             *((int *) res->value)
    );

    DEBUG("Serializing response...\n");
    uint8_t *res_enc = malloc(CERAL_CBOR_DEFAULT_BUFFER_SIZE);
    size_t res_len = ceral_encode_response(&digitsum, res, res_enc, CERAL_CBOR_DEFAULT_BUFFER_SIZE);
    t3_serialized = xtimer_now_usec64();

    // Send the request through UART
    DEBUG("Sending %u bytes...\n", res_len);
    uart_write_size(COM_PORT, res_len);
    uart_write(COM_PORT, res_enc, res_len);
    t4_sent = xtimer_now_usec64();

    close_ceral_request(&digitsum, req);
    close_ceral_response(&digitsum, res);
    printf("Responder Timings: %llu, %llu, %llu, %llu, %llu\n", t0_received, t1_deserialized, t2_executed, t3_serialized, t4_sent);
}

int main(void) {
    int uart_state = uart_init(COM_PORT, STDIO_UART_BAUDRATE, receive_byte, 0);
    if (uart_state != 0) {
        DEBUG("UART setup failed with state %d.\n", uart_state);
    } else {
        DEBUG("All set up.\n");
    }

    while (true) {

        xtimer_msleep(UINT32_MAX);

    }
}
