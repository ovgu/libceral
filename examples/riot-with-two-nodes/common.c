#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <xtimer.h>
#include <periph/uart.h>
#include <ceral.h>
#include "../../../Algorithms/digitsum/i2/digitsum.c"

#ifdef CERAL_ENABLE_DEBUG
#undef ENABLE_DEBUG
#define ENABLE_DEBUG (1)
#endif
#include <debug.h>

// Set baudrate for STDIO/printf
#ifndef STDIO_UART_BAUDRATE
#define STDIO_UART_BAUDRATE 115200
#endif

// Set default UART port for communication
#ifndef COM_PORT
#define COM_PORT UART_DEV(0)
#endif

// Initialize UART buffer for CBOR
#define UART_BUFFER_SIZE CERAL_CBOR_DEFAULT_BUFFER_SIZE
uint8_t buffer[UART_BUFFER_SIZE];
size_t buffer_pos = 0;
uint64_t buffer_size = 0;

// Use printf if verbose logging is enabled
/*#if defined(VERBOSE) || defined(DEBUG)
#define VERBOSEF printf
#else
#define VERBOSEF false && printf
#endif
#ifdef DEBUG
#define DEBUGF printf
#else
#define DEBUGF false && printf
#endif*/

void uart_write_size(uart_t uart, const uint64_t size) {
    CborEncoder encoder;
    uint8_t buffer[11];
    buffer[0] = 0xff; // CBOR: break
    buffer[1] = 0xf7; // CBOR: undefined
    cbor_encoder_init(&encoder, buffer + 2, 9, 0);
    cbor_encode_uint(&encoder, size);
    uart_write(uart, buffer, 11);
}

void receive_data(uint8_t data_size);

#ifdef DUMP_RECEIVED_DATA
uint8_t debug_index = 0;
#endif

void receive_byte(void *arg, uint8_t data) {
    (void)(arg); // don't mark as unused
#ifdef DUMP_RECEIVED_DATA
    //printf("%02x", data);
    printf("%c", data);
    debug_index = debug_index + 1;
    if (debug_index > 32) { printf("\n"); debug_index = 0; }
#endif

    buffer[buffer_pos] = data;
    buffer_pos = buffer_pos + 1;
    if (buffer_pos == 1 && buffer_size == 0 && data != 0xff) {
        buffer_pos = 0;
    } else if (buffer_pos == 2 && buffer_size == 0 && data != 0xf7) {
        buffer_pos = 0;
    } else if (buffer_pos >= 11 && buffer_size == 0) {
        DEBUG("\nTrying to parse size.\n");
        CborError err1, err2;
        CborParser parser;
        CborValue value;
        err1 = cbor_parser_init(buffer + 2, 9, 0, &parser, &value);
        err2 = cbor_value_get_uint64(&value, &buffer_size);
        if (err1 != CborNoError || err2 != CborNoError || buffer_size > UART_BUFFER_SIZE) {
            buffer_pos = 0;
            buffer_size = 0;
            DEBUG("CBOR Error occured: %x, %x.\n", err1, err2);
        } else {
            buffer_pos = 0;
            DEBUG("Size parsed: %llu\n", buffer_size);
        }
    } else if (buffer_size > 0 && (buffer_pos >= buffer_size || buffer_pos >= UART_BUFFER_SIZE)) {
        DEBUG("\nReceived %lld bytes of data.\n", buffer_size);
        uint8_t data_size = buffer_size;
        buffer_pos = 0;
        buffer_size = 0;
        receive_data(data_size);
    }
}