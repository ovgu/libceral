package main

// #cgo LDFLAGS: -lceral -ltinycbor -lm
// #include <ceral.h>
// #include "../../algorithms/digitsum/digitsum.c"
import "C"
import (
    "fmt"
)

func main() {
    req := C.new_digitsum_request(C.int(99996))
    res := C.new_ceral_response(&C.digitsum, req)
    C.ceral_execute(&C.digitsum, req, res)
    defer C.close_ceral_request(&C.digitsum, req)
    defer C.close_ceral_response(&C.digitsum, res)
    if !C.ceral_check(&C.digitsum, req, res) {
        fmt.Printf("Wrong response %d\n", C.digitsum_value(res))
        return
    }
    fmt.Printf("Valid response %d: %+v\n", C.digitsum_value(res), res)
}