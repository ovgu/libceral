#include <ceral.h>
#include "../../algorithms/digitsum/digitsum.h"

int main(void) {
    // Create a request
    CeralRequest *req = new_digitsum_request(71);
    printf("Request %ld with value %d\n", req->request_id, digitsum_params(req));

    // Run the algorithm & handle errors
    CeralResponse *res = new_ceral_response(&digitsum, req);
    ceral_execute(&digitsum, req, res);
    if (!ceral_check(&digitsum, req, res)) {
        printf("Invalid result %d for request %ld\n", digitsum_value(res), res->request_id);
        close_ceral_request(&digitsum, req);
        close_ceral_response(&digitsum, res);
        return 1;
    }

    // Use the data
    printf("Correct result %d for request %ld\n", digitsum_value(res), res->request_id);
    close_ceral_request(&digitsum, req);
    close_ceral_response(&digitsum, res);
}
