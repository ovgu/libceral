#include "../include/ceral.h"
#include "../include/ceral_oidutils.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

size_t ceral_oid_length(uint8_t *oid) {
    if (oid[2] < 0x58) return oid[2] - 0x40;
    else if (oid[2] == 0x58) return oid[3];
    else if (oid[2] == 0x59) return (oid[3] << 8) | oid[4];
    else if (oid[2] == 0x60) return (oid[3] << 16) | (oid[4] << 8) | oid[5];
    else if (oid[2] == 0x61) return (oid[3] << 24) | (oid[4] << 16) | (oid[5] << 8) | oid[6];
    else return 0;
}

size_t ceral_oid_prefix_byte_count(uint8_t *oid) {
    if (oid[2] < 0x58 || oid[2] > 0x61) return 3;
    else return oid[2] - 0x54;
}

size_t ceral_algorithm_id_length(uint8_t *oid) {
    return ceral_oid_prefix_byte_count(oid) + ceral_oid_length(oid);
}

bool ceral_algorithm_id_equals(uint8_t* id_a, uint8_t* id_b) {
    size_t len = ceral_oid_length(id_a) + ceral_oid_prefix_byte_count(id_a);
    if (len != ceral_oid_length(id_b) + ceral_oid_prefix_byte_count(id_b)) return false;
    return memcmp(id_a, id_b, len) == 0;
}

int ceral_algorithm_id_str(char* str, uint8_t* id) {
    int r = 0; size_t l = ceral_algorithm_id_length(id);
    for (size_t i = 0; i < l; i++) r = r + sprintf(str + i*2, "%02x", id[i]);
    str[l*2] = 0;
    return r;
}
