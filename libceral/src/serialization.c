#include "../include/ceral.h"
#include "../include/ceral_oidutils.h"
#include <stddef.h>
#include <stdlib.h>
#include <cbor.h>

size_t ceral_encode_request(CeralAlgorithm *alg, CeralRequest *req, uint8_t *buffer, size_t buffer_size) {
    CborError err = CborNoError, err_test;
    CborEncoder encoder, arrEncoder, mapEncoder;
    cbor_encoder_init(&encoder, buffer, buffer_size, 0);

    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_create_array(&encoder, &arrEncoder, 3));
    CERAL_HANDLE_CBOR_ERROR(cbor_encode_uint(&arrEncoder, req->request_id));
    CERAL_HANDLE_CBOR_ERROR(cbor_encode_tag(&arrEncoder, req->algorithm_id[1]));
    CERAL_HANDLE_CBOR_ERROR(cbor_encode_byte_string(&arrEncoder, &req->algorithm_id[ceral_oid_prefix_byte_count(req->algorithm_id)], ceral_oid_length(req->algorithm_id)));
    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_create_map(&arrEncoder, &mapEncoder, 1));

    CERAL_HANDLE_CBOR_ERROR(cbor_encode_text_stringz(&mapEncoder, "p"));
    if (req->params != NULL) {
        CERAL_HANDLE_CBOR_ERROR(alg->skeleton.cbor_encode_params(&mapEncoder, req->params));
    } else {
        CERAL_HANDLE_CBOR_ERROR(cbor_encode_null(&mapEncoder));
    }

    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_close_container(&arrEncoder, &mapEncoder));
    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_close_container(&encoder, &arrEncoder));

    if (err != CborNoError) goto error;
    return cbor_encoder_get_buffer_size(&encoder, buffer);

    error:
    if (err == CborErrorOutOfMemory) {
        size_t new_size = (buffer_size + cbor_encoder_get_extra_bytes_needed(&encoder));
        *buffer = *((uint8_t *) realloc(buffer, new_size * sizeof(uint8_t)));
        return ceral_encode_request(alg, req, buffer, new_size);
    } else {
        printf("CBOR error: %s", cbor_error_string(err));
        return 0;
    }
}

size_t ceral_encode_response(CeralAlgorithm *alg, CeralResponse *res, uint8_t *buffer, size_t buffer_size) {
    CborError err = CborNoError, err_test;
    CborEncoder encoder, arrEncoder, mapEncoder;
    cbor_encoder_init(&encoder, buffer, buffer_size, 0);

    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_create_array(&encoder, &arrEncoder, 3));
    CERAL_HANDLE_CBOR_ERROR(cbor_encode_uint(&arrEncoder, res->request_id));
    CERAL_HANDLE_CBOR_ERROR(cbor_encode_tag(&arrEncoder, res->algorithm_id[1]));
    CERAL_HANDLE_CBOR_ERROR(cbor_encode_byte_string(&arrEncoder, &res->algorithm_id[ceral_oid_prefix_byte_count(res->algorithm_id)], ceral_oid_length(res->algorithm_id)));
    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_create_map(&arrEncoder, &mapEncoder, 2));

    CERAL_HANDLE_CBOR_ERROR(cbor_encode_text_stringz(&mapEncoder, "v"));
    if (res->value != NULL) {
        CERAL_HANDLE_CBOR_ERROR(alg->skeleton.cbor_encode_value(&mapEncoder, res->value));
    } else {
        CERAL_HANDLE_CBOR_ERROR(cbor_encode_null(&mapEncoder));
    }

    CERAL_HANDLE_CBOR_ERROR(cbor_encode_text_stringz(&mapEncoder, "w"));
    if (res->witness != NULL) {
        CERAL_HANDLE_CBOR_ERROR(alg->skeleton.cbor_encode_witness(&mapEncoder, res->witness));
    } else {
        CERAL_HANDLE_CBOR_ERROR(cbor_encode_null(&mapEncoder));
    }

    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_close_container(&arrEncoder, &mapEncoder));
    CERAL_HANDLE_CBOR_ERROR(cbor_encoder_close_container(&encoder, &arrEncoder));

    if (err != CborNoError) goto error;
    return cbor_encoder_get_buffer_size(&encoder, buffer);

    error:
    if (err == CborErrorOutOfMemory) {
        size_t new_size = (buffer_size + cbor_encoder_get_extra_bytes_needed(&encoder));
        *buffer = *((uint8_t *) realloc(buffer, new_size * sizeof(uint8_t)));
        return ceral_encode_response(alg, res, buffer, new_size);
    } else {
        printf("CBOR Error: %s\n", cbor_error_string(err));
        return 0;
    }
}

#define CERAL_PREPARE_CBOR_PARSER \
    CborError err, err_test;\
    CborParser parser;\
    CborValue value, arrElementValue, mapElementValue;\
    CERAL_HANDLE_CBOR_ERROR(cbor_parser_init(buffer, size, 0, &parser, &value));\
    CERAL_HANDLE_CBOR_ERROR(cbor_value_validate_basic(&value));\
    \
    if (!cbor_value_is_array(&value)) {\
        CERAL_HANDLE_CBOR_ERROR(CborErrorUnsupportedType);\
    }\
    CERAL_HANDLE_CBOR_ERROR(cbor_value_enter_container(&value, &arrElementValue));\
    \
    /* Request ID */\
    if (!cbor_value_is_unsigned_integer(&arrElementValue)) {\
        CERAL_HANDLE_CBOR_ERROR(CborErrorUnsupportedType);\
    }\
    CERAL_HANDLE_CBOR_ERROR(cbor_value_get_uint64(&arrElementValue, &(result->request_id)));\
    CERAL_HANDLE_CBOR_ERROR(cbor_value_advance(&arrElementValue));\
    \
    /* Algorithm ID */\
    uint8_t *algorithm_id = malloc(sizeof(uint8_t) * 7);\
    memcpy(algorithm_id, arrElementValue.ptr, 7);                        \
    CborTag algorithm_id_tag;\
    CERAL_HANDLE_CBOR_ERROR(cbor_value_get_tag(&arrElementValue, &algorithm_id_tag));\
    if (algorithm_id_tag < 0x6e || algorithm_id_tag > 0x70) {\
        CERAL_HANDLE_CBOR_ERROR(CborErrorInappropriateTagForType);\
    }\
    CERAL_HANDLE_CBOR_ERROR(cbor_value_advance(&arrElementValue));\
    size_t algorithm_id_length;\
    CERAL_HANDLE_CBOR_ERROR(cbor_value_calculate_string_length(&arrElementValue, &algorithm_id_length));\
    algorithm_id = realloc(algorithm_id, sizeof(uint8_t) * (ceral_oid_prefix_byte_count(algorithm_id) + algorithm_id_length));\
    CERAL_HANDLE_CBOR_ERROR(cbor_value_copy_byte_string(&arrElementValue, algorithm_id + ceral_oid_prefix_byte_count(algorithm_id), &algorithm_id_length, &arrElementValue));\
    if (!ceral_algorithm_id_equals(alg->algorithm_id, algorithm_id)) {\
        char exp[ceral_algorithm_id_length(alg->algorithm_id)*2+1]; ceral_algorithm_id_str(exp, alg->algorithm_id);\
        char got[ceral_algorithm_id_length(algorithm_id)*2+1]; ceral_algorithm_id_str(got, algorithm_id);\
        printf("Algorithm ID mismatch: expected %s, got %s\n", exp, got);\
        free(algorithm_id);\
        CERAL_HANDLE_CBOR_ERROR((CborError) CeralMissingAlgorithmError)\
    } else {\
        free(algorithm_id);\
        result->algorithm_id = alg->algorithm_id;\
    }\
    \
    if (!cbor_value_is_map(&arrElementValue)) {\
        CERAL_HANDLE_CBOR_ERROR(CborErrorUnsupportedType);\
    }\

CeralRequest *new_ceral_request_from_cbor(CeralAlgorithm *alg, uint8_t *buffer, size_t size) {
    CeralRequest *result = new_ceral_request(NULL, NULL);
    CERAL_PREPARE_CBOR_PARSER

    CERAL_HANDLE_CBOR_ERROR(cbor_value_map_find_value(&arrElementValue, "p", &mapElementValue));
    result->params = NULL;
    if (mapElementValue.type != CborInvalidType) { // TODO: check for null
        CERAL_HANDLE_CBOR_ERROR(alg->skeleton.cbor_decode_params(&mapElementValue, &(result->params)));
    }

    while (arrElementValue.type != CborInvalidType) {
        CERAL_HANDLE_CBOR_ERROR(cbor_value_advance(&arrElementValue));
    }
    CERAL_HANDLE_CBOR_ERROR(cbor_value_leave_container(&value, &arrElementValue));

    return result;

    error:
    printf("CBOR error: %s\n", cbor_error_string(err));
    result->error = err;
    return result;
}


CeralResponse *new_ceral_response_from_cbor(CeralAlgorithm *alg, uint8_t *buffer, size_t size) {
    CeralResponse *result = new_ceral_response(alg, NULL);
    CERAL_PREPARE_CBOR_PARSER

    CERAL_HANDLE_CBOR_ERROR(cbor_value_map_find_value(&arrElementValue, "v", &mapElementValue));
    result->value = NULL;
    if (mapElementValue.type != CborInvalidType) { // TODO: check for null
        CERAL_HANDLE_CBOR_ERROR(alg->skeleton.cbor_decode_value(&mapElementValue, &(result->value)));
    }

    CERAL_HANDLE_CBOR_ERROR(cbor_value_map_find_value(&arrElementValue, "w", &mapElementValue));
    result->witness = NULL;
    if (mapElementValue.type != CborInvalidType) { // TODO: check for null
        CERAL_HANDLE_CBOR_ERROR(alg->skeleton.cbor_decode_witness(&mapElementValue, &(result->witness)));
    }

    while (arrElementValue.type != CborInvalidType) {
        CERAL_HANDLE_CBOR_ERROR(cbor_value_advance(&arrElementValue));
    }
    CERAL_HANDLE_CBOR_ERROR(cbor_value_leave_container(&value, &arrElementValue));

    return result;

    error:
    printf("CBOR error: %s\n", cbor_error_string(err));
    result->error = err;
    return result;
}