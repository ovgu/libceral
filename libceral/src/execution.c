#include "../include/ceral.h"
#include "../include/ceral_oidutils.h"

void ceral_execute(CeralAlgorithm *alg, CeralRequest *req, CeralResponse *res) {

    if (
            !ceral_algorithm_id_equals(alg->algorithm_id, req->algorithm_id) ||
            !ceral_algorithm_id_equals(alg->algorithm_id, res->algorithm_id)
            ) {
        res->error = CeralMissingAlgorithmError;
        return;
    }

    res->error = alg->executor(req, res);
}

bool ceral_check(CeralAlgorithm *alg, CeralRequest *req, CeralResponse *res) {
    if (req->request_id != res->request_id || res->error != CeralNoError || req->error != CeralNoError) {
        return false;
    }
    if (
        !ceral_algorithm_id_equals(alg->algorithm_id, req->algorithm_id) ||
        !ceral_algorithm_id_equals(alg->algorithm_id, res->algorithm_id)
    ) {
        return false;
    }

    return alg->checker(req, res);
}
