#include "../include/ceral.h"
#include "../include/ceral_oidutils.h"
#include <stdlib.h>

CeralRequest *new_ceral_request(CeralAlgorithm *alg, void* params) {
    CeralRequest *req = malloc(sizeof(CeralRequest));
    if (alg != NULL) {
        req->algorithm_id = alg->algorithm_id;
        req->request_id = CERAL_GENERATE_REQUEST_ID;
    }
    if (params != NULL) {
        req->params = params;
    }
    req->error = CeralNoError;
    return req;
}

CeralResponse *new_ceral_response(CeralAlgorithm *alg, CeralRequest *req) {
    CeralResponse *res = malloc(sizeof(CeralResponse));
    if (req != NULL) {
        res->algorithm_id = alg->algorithm_id;
        res->request_id = req->request_id;
    } else {
        res->algorithm_id = NULL;
        res->request_id = 0;
    }
    res->value = NULL;
    res->witness = NULL;
    res->error = CeralNoError;
    return res;
}

void close_ceral_response(CeralAlgorithm *alg, CeralResponse *res) {
    if (alg != NULL) {
        alg->skeleton.close_response(res);
    } else {
        if (res->value != NULL) free(res->value);
        if (res->witness != NULL) free(res->witness);
    }
    free(res);
}

void close_ceral_request(CeralAlgorithm *alg, CeralRequest *req) {
    if (alg != NULL) {
        alg->skeleton.close_request(req);
    } else {
        if (req->params != NULL) free(req->params);
    }
    free(req);
}
