#ifndef LIBCERAL
#define LIBCERAL

#ifndef CERAL_DISABLE_CBOR
#include <cbor.h>
#else
typedef void CborError;
typedef void CborEncoder;
typedef void CborValue;
#define CborNoError 0
#endif

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

/**
 * CERAL_GENERATE_REQUEST_ID shall return a random uint64 number to identify a request.
 * This is by default done by using the `rand()` function, but ideally should identify the system somehow
 * together with an incremental request ID.
 */
#define CERAL_GENERATE_REQUEST_ID rand()

#ifndef CERAL_DISABLE_CBOR
/**
 * CERAL_HANDLE_CBOR_ERROR(CborError x) uses the variables `err` and `err_test`, and uses `goto error` to
 * jump to an error handler if there's a non-recoverable error. A usual use case could look like this:
 *
 * CborError err = CborNoError, err_test;
 * CERAL_HANDLE_CBOR_ERROR(...);
 * error:
 * return err;
 */
#define CERAL_HANDLE_CBOR_ERROR(x) \
    err_test = x; \
    if (err_test != CborNoError) { \
        err = err_test; \
        if (err != CborErrorOutOfMemory) goto error; \
    }
#endif

/**
 * CERAL_ALGORITHM(name, oid) fills the fields of a CeralAlgorithm according to the usual naming convention.
 * It requires a name (that's used for the method names) and an algorithm id.
 *
 * Usual usage would look like this:
 * CeralAlgorithm myalgorithm = { CERAL_ALGORITHM(myalgorithm, "\xD8\x70\x45\x83\xc2\x75\x1f\x01") };
 */
#define CERAL_ALGORITHM(name, oid) \
    .algorithm_id = (uint8_t*) oid,\
    \
    .skeleton = {\
        .cbor_encode_params = name ## _encode_params,\
        .cbor_decode_params = name ## _decode_params,\
        .cbor_encode_value = name ## _encode_value,\
        .cbor_decode_value = name ## _decode_value,\
        .cbor_encode_witness = name ## _encode_witness,\
        .cbor_decode_witness = name ## _decode_witness,\
        \
        .close_request = close_ ## name ## _request,\
        .close_response = close_ ## name ## _response,\
    },\
    .executor = name ## _executor,\
    .checker = name ## _checker,
#ifdef CERAL_DISABLE_CBOR
#undef CERAL_ALGORITHM
#define CERAL_ALGORITHM(name, oid) \
    .algorithm_id = (uint8_t*) oid,\
    \
    .skeleton = {\
        .close_request = close_ ## name ## _request,\
        .close_response = close_ ## name ## _response,\
    },\
    .executor = name ## _executor,\
    .checker = name ## _checker,
#endif

//********************//
// REQUEST & RESPONSE //
//********************//

/**
 * An error that could occur during an execution or deserialization. It might also be a CborError if the values are different.
 */
typedef enum CeralError {
    /**
     * The action succeeded without issues.
     */
    CeralNoError = CborNoError,
    /**
     * The preconditions weren't matched.
     */
    CeralIllegalInputError = 31400,
    /**
     * The algorithm is missing, either because the OIDs don't match or because the executor wasn't compiled.
     */
    CeralMissingAlgorithmError = 31404,
    /**
     * An unknown algorithm-internal
     *
     *
     * error occured.
     */
    CeralAlgorithmInternalError = 31500,
} CeralError;

/**
 * A request designated for a certifying algorithm.
 */
typedef struct CeralRequest {
    /**
     * The name of the algorithm, optionally with a version.
     */
    uint8_t *algorithm_id;
    /**
     * A random (or incremental when used in a thread-safe manner on a single node) request ID.
     */
    uint64_t request_id;
    /**
     * The parameters passed to the executor.
     */
    void *params;
    /**
     * After parsing a request or response, this contains the CBOR error and MUST be checked against CborNoError.
     */
    CeralError error;
} CeralRequest;

/**
 * A response from a certifying algorithm, containing the calculated value, as well as the request data.
 */
typedef struct CeralResponse {
    /**
     * A CBOR-encoded OID that uniquely identifies the algorithm.
     * You can use https://momar.codeberg.page/oid-tool/?format=cbor-esc&oid=1.3.6.1.4.1.57717.31.1 to build this id.
     */
    uint8_t *algorithm_id;
    /**
     * A random (or incremental when used in a thread-safe manner on a single node) request ID.
     */
    uint64_t request_id;
    /**
     * The return value calculated by the certifying algorithm.
     */
    void *value;
    /**
     * The matching witness calculated by the certifying algorithm.
     */
    void *witness;
    /**
     * After parsing a request or response, this contains the CBOR error and MUST be checked against CborNoError.
     */
    CeralError error;
} CeralResponse;

/*
 * A set of (de)serialization & memory management features for a specific algorithm, that acts as the base
 * for its implementation.
 */
typedef struct CeralAlgorithmSkeleton {
    /**
     * Encode a requests's parameters to CBOR.
     * @param encoder The encoder that should be used.
     * @param params The void* pointer to the field that should be encoded.
     * @return The error that occured, or CborNoError if everything went fine.
     */
    CborError (*cbor_encode_params)(CborEncoder *encoder, void *params);

    /**
     * Decode a request's parameters from CBOR.
     * @param encoded_value The value that holds the CBOR data.
     * @param params The target void** pointer that should be allocated and set to the parsed contents of encoded_value.
     * @return The error that occured, or CborNoError if everything went fine.
     */
    CborError (*cbor_decode_params)(CborValue *encoded_value, void **params);

    /**
     * Encode a response value to CBOR.
     * @param encoder The encoder that should be used.
     * @param value The void* pointer to the field that should be encoded.
     * @return The error that occured, or CborNoError if everything went fine.
     */
    CborError (*cbor_encode_value)(CborEncoder *encoder, void *value);

    /**
     * Decode a response value from CBOR.
     * @param encoded_value The value that holds the CBOR data.
     * @param value The target void** pointer that should be allocated and set to the parsed contents of encoded_value.
     * @return The error that occured, or CborNoError if everything went fine.
     */
    CborError (*cbor_decode_value)(CborValue *encoded_value, void **value);

    /**
     * Encode a response witness to CBOR.
     * @param encoder The encoder that should be used.
     * @param witness The void* pointer to the field that should be encoded.
     * @return The error that occured, or CborNoError if everything went fine.
     */
    CborError (*cbor_encode_witness)(CborEncoder *encoder, void *witness);

    /**
     * Decode a response witness from CBOR.
     * @param encoded_value The value that holds the CBOR data.
     * @param witness The target void** pointer that should be allocated and set to the parsed contents of encoded_value.
     * @return The error that occured, or CborNoError if everything went fine.
     */
    CborError (*cbor_decode_witness)(CborValue *encoded_value, void **witness);

    /**
     * Free all memory that is allocated by either new_myalgorithm_request() or cbor_decode_params().
     * @param req The request object that holds the parameters.
     */
    void (*close_request)(CeralRequest *req);

    /**
     * Free all memory that is allocated by either executor() or cbor_decode_value() and cbor_decode_witness().
     * @param res The response object that holds the value and the witness.
     */
    void (*close_response)(CeralResponse *res);
} CeralAlgorithmSkeleton;

/**
 * A certifying algorithm with all functions required to build, execute and validate a request.
 */
typedef struct CeralAlgorithm {
    /**
     * An CBOR-encoded ASN.1 OID (RFC 9090) that uniquely identifies the algorithm.
     * You can use https://momar.codeberg.page/oid-tool/?format=cbor-esc&oid=1.3.6.1.4.1.57717.31.0 to format an OID that way, and https://codeberg.org/ovgu/ceral-registry to request an OID for your algorithm.
     */
    uint8_t *algorithm_id;

    /**
     * The skeleton of the algorithm, holding memory management and CBOR (de-)serialization functions.
     */
    struct CeralAlgorithmSkeleton skeleton;

    /**
     * A checker implementation that returns true if (and only if) the response is valid for the given request.
     * @param req The request that was used to execute the algorithm.
     * @param res The result returned by ceral_execute().
     * @return true if (and only if) the response is valid for the given request.
     */
    bool (*checker)(CeralRequest *req, CeralResponse *res);

    /**
     * An executor implementation that runs the algorithm against the request and returns the value and a witness.
     * @param req The request that should be evaluated.
     * @param res The response where value and witness should be written to - you may allocate memory here, but make sure that it can be cleaned up with skeleton.close_response()!
     * @return An error if one occured, otherwise CeralNoError.
     */
    CeralError (*executor)(CeralRequest *req, CeralResponse *res);
} CeralAlgorithm;

//*******************//
// ENCODING/DECODING //
//*******************//

/**
 * A sensible default buffer size for CBOR-encoded data.
 */
#define CERAL_CBOR_DEFAULT_BUFFER_SIZE 128 * sizeof(uint8_t)

/**
 * Encode a request for transmission over any binary protocol.
 * @param alg The algorithm implementation to use.
 * @param req The request to encode.
 * @param buffer The buffer where to write the result.
 * @param buffer_size The size of buffer.
 * @return The length of the encoded request in bytes.
 */
size_t ceral_encode_request(CeralAlgorithm *alg, CeralRequest *req, uint8_t *buffer, size_t buffer_size);

/**
 * Encode a response for transmission over any binary protocol.
 * @param alg The algorithm implementation to use.
 * @param res The response to encode.
 * @param buffer The buffer where to write the result.
 * @param buffer_size The size of buffer.
 * @return The length of the encoded request in bytes.
 */
size_t ceral_encode_response(CeralAlgorithm *alg, CeralResponse *res, uint8_t *buffer, size_t buffer_size);

/**
 * Decode a request or response transmitted via a binary protocol for further processing.
 * @param alg The algorithm implementation to use.
 * @param buffer The request or response in its CBOR-encoded binary format.
 * @param size The size of buffer.
 * @return The decoded response, containing the request.
 */
CeralRequest* new_ceral_request_from_cbor(CeralAlgorithm *alg, uint8_t *buffer, size_t size);

/**
 * Decode a request or response transmitted via a binary protocol for further processing.
 * @param alg The algorithm implementation to use.
 * @param buffer The request or response in its CBOR-encoded binary format.
 * @param size The size of buffer.
 * @return The decoded response, containing the request.
 */
CeralResponse* new_ceral_response_from_cbor(CeralAlgorithm *alg, uint8_t *buffer, size_t size);

//***********//
// EXECUTION //
//***********//

/**
 * Execute a request with the matching executor locally.
 * @param alg The algorithm implementation to use.
 * @param req The request to execute.
 * @param res The response where the value and witness should be written to.
 */
void ceral_execute(CeralAlgorithm *alg, CeralRequest *req, CeralResponse *res);

/**
 * Validate a response from a certifying algorithm using the matching checker.
 * @param alg The algorithm implementation to use.
 * @param req The request that was used to produce the response.
 * @param res The response to check, must include the request with all parameters.
 * @return true if the checker deemed the result valid.
 */
bool ceral_check(CeralAlgorithm *alg, CeralRequest *req, CeralResponse *res);

/**
 * Allocate a new request on the heap. Calling ceral_close_request() later is required!
 * @param algorithm The algorithm implementation to use.
 * @param params The input parameters to use for the algorithm.
 */
CeralRequest *new_ceral_request(CeralAlgorithm *algorithm, void* params);

/**
 * Allocate a new response on the heap. Calling ceral_close_response() later is required!
 * @param alg The algorithm implementation to use.
 * @param req The request to create a response for.
 */
CeralResponse *new_ceral_response(CeralAlgorithm *alg, CeralRequest *req);

/**
 * Free all resources allocated by a response and its attached response.
 * @param alg The algorithm implementation to use.
 * @param res The response of a certifying algorithm.
 */
void close_ceral_response(CeralAlgorithm *alg, CeralResponse *res);

/**
 * Free all resources allocated by a request.
 * @param alg The algorithm implementation to use.
 * @param req The request of a certifying algorithm.
 */
void close_ceral_request(CeralAlgorithm *alg, CeralRequest *req);

/**
 * Check if two algorithm IDs are equal.
 * @param id_a The first ID
 * @param id_b The second ID
 * @return true if the IDs are equal
 */
bool ceral_algorithm_id_equals(uint8_t* id_a, uint8_t* id_b);

#endif //LIBCERAL
