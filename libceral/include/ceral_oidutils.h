#ifndef LIBCERAL_OIDUTILS
#define LIBCERAL_OIDUTILS
#include <stddef.h>

/**
 * Get the length of a CBOR-encoded OID (without the prefix bytes).
 * @param oid The CBOR-encoded OID according to RFC 9090.
 * @return The length of the OID.
 */
size_t ceral_oid_length(uint8_t *oid);

/**
 * Get the number of bytes to skip in a CBOR OID from its length.
 * @param oid The CBOR-encoded OID according to RFC 9090.
 * @return The length of the OID prefix.
 */
size_t ceral_oid_prefix_byte_count(uint8_t *oid);

/**
 * Get the length of an algorithm ID in bytes, consisting of a full RFC 9090 encoded OID.
 * @param id The CBOR-encoded OID according to RFC 9090.
 * @return The length of the ID, effectively the sum of ceral_oid_prefix_byte_count(id) and ceral_oid_length(id).
 */
size_t ceral_algorithm_id_length(uint8_t* id);

/**
 * Write the OID to a string (which must have a large enough length) as hexadecimal bytes (so, 2*ceral_oid_length(id)+1).
 * @param str The string to write the OID to.
 * @param id The ID to write into the string.
 * @return The number of copied bytes (and thus normally the length of the stringified OID).
 */
int ceral_algorithm_id_str(char* str, uint8_t* id);

#endif //LIBCERAL_OIDUTILS
