all:
	cd libceral && make $(target)
	export LD_LIBRARY_PATH="$$LD_LIBRARY_PATH:$$PWD/libceral" ;\
	export C_INCLUDE_PATH="$$C_INCLUDE_PATH:$$PWD/libceral/include" ;\
	for e in examples/*; do cd "$$e" && make $(target); cd ../..; done
clean:
	make all target=clean
